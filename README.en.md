# chuchuguai

#### Introduction
This is a godot open source community online game demo, expanded from the multiplayer online game template.

#### Software Architecture
1. Use godot open source community multiplayer bomber and websocket chat demo to build.

2. Use gdscript for programming.

3. Use godot4.2.2 for debugging.


#### installation tutorial

1.windows: After the project is downloaded and decompressed, open the chuchu.exe application file in chuchu-exe.

2.Linux (if supported) : After downloading the project and decompressing it, open the chuchu.x86_64 application file in chuchu-linux.


#### Usage instructions

1. Local debugging:

1) After the project is downloaded and decompressed, open the godot engine, scan the folder where the project is located, check the project address, and edit the project (double-click to open by default).

2) In the same window, the host button creates the server, then click the start button to start the game, and the esc key exits.

3) In another window, before the previous window start, the join button creates the client connection, and the esc key exits.

4) After multiple Windows are connected, the server starts the game and then exits, and after other clients exit, the original client creates the server, an error will occur, and the peer_id cannot be checked.

5) There are two common scripts: gamestate and tscript. gamestate mainly includes network connection and level message processing, and tscript mainly includes level tools.

6) There is a level: world, 0, 1, 2, 3.


2. Local application test:

1) After opening the application file, the interface displays the port number input bar, ip address input bar, host button, and join button.

2) The server (homeowner) can select the level.

3) When applying the test, the server and client can be changed after the game ends or the game exits.


3. Other instructions:

1) In this project, the actual level requirements do not require the establishment of a tcp session.

2) The label prompt after a level is selected in the homeowner interface can also be synced via enetmultiplayer.

3) If enetmultiplayer is used, intra-level synchronization needs attention, synchronization nodes should be created, and synchronization properties should be added.

4) If the game uses simple network communication, pay attention to whether the MultiplayerSynchronizer has been bound to the root path during debugging; otherwise, it cannot load normally.


#### Contribute