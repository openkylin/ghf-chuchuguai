# chuchuguai

#### 介绍
这是一个基于godot开源社区联机游戏demo，拓展而来的多人联机游戏模板。

#### 软件架构
1.使用godot开源社区multiplayer bomber以及websocket chat demo进行搭建。

2.使用gdscript进行编程。

3.调试时使用godot4.2.2版本。


#### 安装教程

1.windows：将项目下载并解压完成后，直接打开chuchu-exe中的chuchu.exe应用文件。

2.Linux（如果支持）：将项目下载并解压完成后，直接打开chuchu-linux中的chuchu.x86_64应用文件。


#### 使用说明

1.本地调试：

    1）将项目下载并解压完成后，打开godot引擎，扫描项目所在文件夹，核对项目地址后编辑项目（默认双击打开）。

    2）同一个窗口内，host按钮创建服务端，再点击start按钮开始游戏，esc键退出。

    3）另一个窗口内，前一个窗口start之前，join按钮创建客户端连接，esc键退出。

    4）多个窗口连接完成后，服务端开始游戏再退出，并且其他客户端退出后，原先的客户端创建服务端，会发生报错，无法通过peer_id校验。

    5）有两个公共脚本：gamestate、tscript。其中gamestate主要包含网络连接和关卡报文处理，tscript主要包含关卡内工具。

    6）有个关卡：world、0、1、2、3。


2.本地应用测试：

    1）打开应用文件之后，界面有端口号输入栏、ip地址输入栏、host按钮、join按钮。

    2）服务端（房主）可以选择关卡。

    3）应用测试时，可以在游戏结束或退出游戏后，变更服务端和客户端。


3.其他说明：

    1）在该项目中，实际关卡需求并不需要建立tcp会话。

    2）房主界面中关卡选择后的label提示也可以通过enetmultiplayer进行同步。

    3）如果使用enetmultiplayer，关卡内同步需要注意，应创建同步节点，添加同步属性。

    4）如果游戏采用简易网络通信，调试时，需要注意同步节点MultiplayerSynchronizer是否已经绑定root path，否则无法正常加载。


#### 参与贡献


